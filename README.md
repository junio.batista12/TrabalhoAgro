# TrabalhoAgro
## Introdução
> Esse programa consiste em retornar as sementes cujas condições atuais estejam
propícias para determinadas ações dentro do âmbito agronômico. Até a versão
atual, o programa leva em consideração somente o mês e a temperatura.

## Instalação e execução
> Para instalar o programa e executar, basta rodar os comandos a seguir no terminal (estando no
diretório do programa):
```sh
$ make
$ ./main
```
Após isso o programa irá executar normalmente. Para removê-lo, basta executar:
```sh
$ make clean #Isso irá somente remover o executavel
$ make cleanall #Isso irá remover também o código fonte
```